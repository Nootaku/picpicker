module.exports = {
  packagerConfig: {
    icon: './src/dist/icon/icon'
  },
  rebuildConfig: {},
  makers: [
    {
      name: '@electron-forge/maker-squirrel',
      platforms: ['win32'],
      config: {
        name: 'picpicker_installer',
        setupIcon: './src/dist/icon/icon.ico',
      },
    },
    // {
    //   name: '@electron-forge/maker-zip',
    //   platforms: ['darwin'],
    //   config: {
    //     icon: './src/dist/icon/icon.icns'
    //   }
    // },
    {
      name: '@electron-forge/maker-deb',
      platforms: ['linux'],
      config: {
        setupIcon: './src/dist/icon/icon.png',
        options: {
          maintainer: 'Max Wattez',
          homepage: 'https://picpicker.site',
          icon: './src/dist/icon/icon.png'
        }
      },
    }
  ],
  buildIdentifier: "vapp_version"
};
