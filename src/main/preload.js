const { contextBridge, ipcRenderer } = require('electron');

contextBridge.exposeInMainWorld('versions', {
  node: () => process.versions.node,
  chrome: () => process.versions.chrome,
  electron: () => process.versions.electron,
  ping: () => ipcRenderer.invoke('ping'),
});

contextBridge.exposeInMainWorld('api', {
  // Actions sent from the renderer to the main
  selectDir: () => ipcRenderer.invoke('dialog:directory'),
  getDirImages: (directory_path) => ipcRenderer.send('fs:directory-images', directory_path),
  dirImages: (callback) => ipcRenderer.once('fs:directory-images-reply', callback),
  clickado: (image_path, output_directory, image_name) => ipcRenderer.send('execute:copy-image', image_path, output_directory, image_name),
  clickadoResponse: (callback) => ipcRenderer.once('execute:reply', callback),

  // All actions from accelerators
  selectInput: (callback) => ipcRenderer.on('action:select-input', callback),
  selectOutput: (callback) => ipcRenderer.on('action:select-output', callback),
  nextImage: (callback) => ipcRenderer.on('action:next-image', callback),
  previousImage: (callback) => ipcRenderer.on('action:previous-image', callback),
  rotateLeft: (callback) => ipcRenderer.on('action:rotate-left', callback),
  rotateRight: (callback) => ipcRenderer.on('action:rotate-right', callback),
  doExecute: (callback) => ipcRenderer.on('action:execute', callback),
  toggleRename: (callback) => ipcRenderer.on('action:toggle-rename', callback)
});