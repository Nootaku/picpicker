const { Menu, dialog, BrowserWindow } = require('electron');
const path = require('path');

const software_description = `
This software is a personal project of Maxime WATTEZ.
Feel free to use and modify it according to the GNU General Public License (V3).
How to use PicPicker:
    - Select an input directory.
    - Select an output directory.
    - Use the left and right arrows to navigate through the folder's pictures.
    - Use the up and down arrows to rotate the picture.
    - Press space to copy the current picture to the output directory.
`

// Window & Linux
function buildMenu(app) { // , isMacOs
    const windows_linux_menu = [
        {
            label: 'App',
            submenu: [
                {
                    role: 'about',
                    click: () => dialog.showMessageBox(
                        options={
                            type:'info',
                            title: 'ABOUT PICPICKER', // title
                            message: `PicPicker - Version ${app.getVersion()}`, // header
                            detail: software_description,
                            icon: path.join(path.basename(__dirname), 'public', '50x50.png'),
                            buttons:["Ok"]
                        }
                    )
                },
                { type: 'separator' },
                { role: 'togglefullscreen' },
                { role: 'minimize' },
                { role: 'quit' }
            ]
        }, {
            label: 'Actions',
            submenu: [
                { label: 'Select source', click: () => app.emit('accelerator:select-input') },
                { label: 'Select destination', click: () => app.emit('accelerator:select-output') },
                { type: 'separator' },
                { label: 'Next picture', accelerator: 'Right', click: () => app.emit('accelerator:next') },
                { label: 'Previous picture', accelerator: 'Left', click: () => app.emit('accelerator:previous') },
                { label: 'Rotate left', accelerator: 'Up', click: () => app.emit('accelerator:rotate-left') },
                { label: 'Rotate right', accelerator: 'Down', click: () => app.emit('accelerator:rotate-right') },
                { label: 'Copy', accelerator: 'Space', click: () => app.emit('accelerator:execute') }
            ]
        }, {
            label: 'Options',
            submenu: [
                { label: 'Toggle Rename', accelerator: 'CommandOrControl+R', click: () => app.emit('accelerator:toggle-rename') }
            ]
        }
    ]

    return windows_linux_menu
}


// Build menu
const picpicker_menu = (app) => Menu.buildFromTemplate(buildMenu(app));


exports.picpicker_menu = picpicker_menu;