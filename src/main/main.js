const { app, BrowserWindow, ipcMain, ipcRenderer, Menu } = require('electron');
const path = require('path');
const { contextIsolated } = require('process');

// Menu
const { picpicker_menu } = require(path.join(__dirname, 'templates', 'menu'));

// Functions
const { getDirectoryImages, openDirectory, copyImage } = require(
  path.join(__dirname, 'methods', 'directory')
);

if (require('electron-squirrel-startup')) app.quit();
app.disableHardwareAcceleration();

const createWindow = () => {
  // Main Window configuration
  const mainWindow = new BrowserWindow({
    width: 1200,
    height: 800,
    backgroundColor: "#23787D",
    icon: path.join(__dirname, "public", "50x50.png"),
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: false,
      contextIsolation: true,
    },
  })

  // All shortcuts and accelerators
  app.on('accelerator:select-input', () => {mainWindow.webContents.send('action:select-input')});
  app.on('accelerator:select-output', () => {mainWindow.webContents.send('action:select-output')});
  app.on('accelerator:next', () => {mainWindow.webContents.send('action:next-image')});
  app.on('accelerator:previous', () => {mainWindow.webContents.send('action:previous-image')});
  app.on('accelerator:rotate-left', () => {mainWindow.webContents.send('action:rotate-left')});
  app.on('accelerator:rotate-right', () => {mainWindow.webContents.send('action:rotate-right')});
  app.on('accelerator:execute', () => {mainWindow.webContents.send('action:execute')});
  app.on('accelerator:toggle-rename', () => {mainWindow.webContents.send('action:toggle-rename')});

  // Load index.html
  mainWindow.loadFile(path.join(app.getAppPath(), 'src', 'renderer', 'index.html'));

  // Show dev tools for debug
  // mainWindow.webContents.openDevTools();
}

// ------------------------------------------------------------------------------

app.whenReady().then(() => {

  // Client event handlers
  ipcMain.handle('dialog:directory', () => openDirectory());
  ipcMain.on(
    'fs:directory-images',
    (event, directory_path) => getDirectoryImages(event, directory_path)
  );
  ipcMain.on(
    'execute:copy-image',
    (event, image_path, output_directory, image_name) => copyImage(event, image_path, output_directory, image_name)
  );

  // Menus
  Menu.setApplicationMenu(picpicker_menu(app));

  // Create Main Window
  createWindow();

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow();
    }
  });
});

// Close application
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});