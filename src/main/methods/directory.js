const { readdir, copyFile } = require('fs');
const { dialog } = require('electron');
const { basename, dirname, extname, join } = require('path');
const { openFileOptions } = require(
  join(dirname(__dirname), 'templates', 'dialog')
);

// Open File Dialog and return selected directory Path
function openDirectory(window) {
  const directory_path = dialog.showOpenDialogSync(openFileOptions);
  // const directory_path = dialog.showOpenDialog(window, openFileOptions);
  if (directory_path !== undefined) {
    return directory_path
  } else {
    return null
  }
}


function getDirectoryImages(event, directory_path) {
  // Return list of paths of all files in directory
  const ALLOWED_EXTENSIONS = [".jpg", ".png", ".jpeg", ".JPG", ".PNG", ".JPEG"];
  const image_list = []

  readdir(
    directory_path,
    (err, files) => {
      // Error handler
      if (err) {
        console.error("ERROR: `fs.readdir` in file ./src/main/methods/directory.js");
        console.error(err);
        return
      }

      // Listing files
      files.forEach(file => {
        // only keep file in list if the extension is in the allowed extensions
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/includes
        if (ALLOWED_EXTENSIONS.includes(extname(file))) {
          image_list.push(join(directory_path, file));
        }
      });

      // Returning populated array directly to the renderer
      event.reply('fs:directory-images-reply', image_list)
  });
}


function copyImage(event, source, destination_dir, image_name) {
  // multiple choices:
  // .. Promise: https://nodejs.org/dist/latest-v18.x/docs/api/fs.html#fspromisescopyfilesrc-dest-mode
  // .. Callback: https://nodejs.org/dist/latest-v18.x/docs/api/fs.html#fscopyfilesrc-dest-mode-callback
  // .. Synchronous: https://nodejs.org/dist/latest-v18.x/docs/api/fs.html#fscopyfilesyncsrc-dest-mode

  const destination = join(destination_dir, image_name);

  copyFile(
    source,
    destination,
    function (err) {
      // Error handler
      if (err) {
        console.error("ERROR: `fs.copyFile` in file ./src/main/methods/directory.js");
        console.error(err);
        event.reply('execute:reply', false);
      }

      // Returning populated array directly to the renderer
      event.reply('execute:reply', true);
    }
  );

  try {
    copyFileSync(source, destination);
    return true
  } catch {
    return false
  }
}


exports.openDirectory = openDirectory;
exports.getDirectoryImages = getDirectoryImages;
exports.copyImage = copyImage;