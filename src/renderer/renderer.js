// Global variables

const LIST_OF_PICTURES = [];
let IMAGE_ROTATION_ANGLE = 0;
let COPY_COUNTER = 0;

// DOM Elements

const _image_container = document.getElementById('pictureContainer');
const _displayed_image = document.getElementById('picture');
const _previous_image_button = document.getElementById('previousPicture');
const _next_image_button = document.getElementById('nextPicture');
const _input_directory = document.getElementById('inputDirectory');
const _output_directory = document.getElementById('outputDirectory');
const _input_directory_button = document.getElementById('inputDirectoryButton');
const _output_directory_button = document.getElementById('outputDirectoryButton');
const _rename_div = document.getElementById('renameFileDiv');
const _rename_input = document.getElementById('fileRenameInput');
const _execute_button = document.getElementById('clickado');
const _counter_pill = document.getElementById('counter');
const _success_alert = document.getElementById('success');
const _error_alert = document.getElementById('error');
const _error_alert_button = document.getElementById('error-btn');

// Event Listeners

_previous_image_button.addEventListener('click', () => displayNextPicture(false));
_next_image_button.addEventListener('click', () => displayNextPicture(true));
_input_directory_button.addEventListener('click', () => selectDirectory(true));
_output_directory_button.addEventListener('click', () => selectDirectory(false));
_execute_button.addEventListener('click', () => execute());
_error_alert_button.addEventListener('click', () => disperseErrorAlert);

// ---------------------------------------------------------------------------
// FUNCTIONS
// ---------------------------------------------------------------------------

// DIRECTORY SELECTION
// -------------------
window.api.selectInput((_event) => selectDirectory(true));
window.api.selectOutput((_event) => selectDirectory(false));

function getDirectoryImages(directory_path) {
  // Empty LIST_OF_PICTURES array
  LIST_OF_PICTURES.length = 0;

  // Make the call to get images
  window.api.getDirImages(directory_path);

  // Populate array with images of selected directory
  window.api.dirImages((_event, directory_image_array) => {
    directory_image_array.forEach(image => LIST_OF_PICTURES.push(image));
    _displayed_image.src = LIST_OF_PICTURES[0];
    update_rename_input();
    enableCopyButton();
  })
}

async function selectDirectory(is_input) {
  // Open File Dialog and return path to selected directory
  const directory_path_array = await window.api.selectDir();

  if (is_input) {
    _input_directory.value = directory_path_array[0];
    getDirectoryImages(directory_path_array[0]);
    _input_directory_button.blur();
  } else {
    _output_directory.value = directory_path_array[0];
    _output_directory_button.blur();
    enableCopyButton();
  }
}


// TOGGLE RENAME INPUT
// -------------------
window.api.toggleRename((_event) => toggleRenameInput());

function getCurrentPictureName() {
  const current_picture_path = _displayed_image.getAttribute('src');
  const current_picture_name = current_picture_path.split(/[\\/]/).pop();
  return current_picture_name
}

function update_rename_input() {
  _rename_input.value = getCurrentPictureName();
}

function toggleRenameInput() {
  _rename_div.classList.toggle('visible')
  update_rename_input()
}


// COPY PICTURE
// ------------
window.api.doExecute((_event) => execute());

function enableCopyButton() {
  const input = _input_directory.value;
  const output = _output_directory.value;

  if (input && input != "Select file" && output && output != "Select file") {
    _execute_button.disabled = false;
    _execute_button.classList.remove('disabled')
  } else if (!_execute_button.classList.contains('disabled')) {
    _execute_button.classList.add('disabled')
  }
}

function execute() {
  const current_picture_path = _displayed_image.getAttribute('src');
  const output_directory = _output_directory.value;
  const image_name = _rename_input.value;

  if (output_directory && output_directory != "Select file") {
    window.api.clickado(current_picture_path, output_directory, image_name);
    window.api.clickadoResponse((_event, is_success) => {

      if (is_success) {
        // Increment counter
        COPY_COUNTER ++
        _counter_pill.innerText = COPY_COUNTER;

        // Display green success box
        _success_alert.classList.toggle('visible');
        setTimeout(function () {
          _success_alert.classList.toggle('visible');
        }, 1100);

      } else {
        // Display red error box
        _error_alert.classList.toggle('visible');
      }
    })
  }
}

function disperseErrorAlert() {
  _error_alert.classList.toggle('visible');
}

// PICTURE PARSING
// ---------------
window.api.nextImage((_event) => displayNextPicture(true));
window.api.previousImage((_event) => displayNextPicture(false));

function displayPictureByIndex(index) {
  if (LIST_OF_PICTURES.length > 0) {
    if (LIST_OF_PICTURES[index]) {
      _displayed_image.src = LIST_OF_PICTURES[index];
      update_rename_input()
    }
  } else {
    console.log('No directory selected.')
  }
}

function displayNextPicture(isNext) {
  const current_picture_path = _displayed_image.getAttribute('src');
  const current_picture_index = LIST_OF_PICTURES.indexOf(current_picture_path);
  if (isNext) {
    displayPictureByIndex(current_picture_index + 1);
  } else {
    displayPictureByIndex(current_picture_index - 1);
  };

  resetRotation();
}

// PICTURE ROTATION
// ----------------
window.api.rotateLeft((_event) => rotateImage(false));
window.api.rotateRight((_event) => rotateImage(true));

function rotateImage(clockwise) {
  _image_container.classList.toggle('rotated');
  _displayed_image.classList.toggle('rotated');

  if (clockwise && IMAGE_ROTATION_ANGLE < 270) {
    IMAGE_ROTATION_ANGLE += 90;

  } else if (!clockwise && IMAGE_ROTATION_ANGLE > -270) {
    IMAGE_ROTATION_ANGLE -= 90;

  } else {
    IMAGE_ROTATION_ANGLE = 0;
  }

  _displayed_image.style.transform = `rotate(${IMAGE_ROTATION_ANGLE}deg)`;
}

function resetRotation() {
  IMAGE_ROTATION_ANGLE = 0
  // picture.style.transform = "none";
  _displayed_image.style.transform = `rotate(${IMAGE_ROTATION_ANGLE}deg)`;
}

