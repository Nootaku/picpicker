# ![PicPicker Logo](./documentation/images/picpicker_2_light.png)

[![Licence](https://img.shields.io/badge/Licence-GNU_General_Public_License_(V3)-lightgrey?style=for-the-badge&logo=GNU&logoColor=white)](LICENCE.md)<br/>
[![Developer](https://img.shields.io/badge/Developer-Nootaku-informational?style=for-the-badge&logo=GitLab&logoColor=white)](https://gitlab.com/Nootaku)<br/>
![Update](https://img.shields.io/badge/Last_Update-Feb_26,_2024-green?style=for-the-badge)<br/>
![Version](https://img.shields.io/badge/Version-1.2.0-yellow?style=for-the-badge)


## What is PickPicker ?

_PicPicker_ is a simple [Electron](https://www.electronjs.org/) application that allows the user to copy pictures from one directory to another with the click of a button.

Put simply, it is an image viewer with a few extra buttons.

### Download

_PicPicker_ is availble on Windows and Linux. Refer to the [releases](https://gitlab.com/Nootaku/picpicker/-/releases) page for specific verions.

- [![Windows](https://img.shields.io/badge/latest_version-blue?style=plastic&logo=windows&logoColor=blue&label=Windows)](https://gitlab.com/Nootaku/picpicker/-/jobs/3408074572/artifacts/raw/out/v1.2.0/make/squirrel.windows/x64/PicPicker-1.2.0%20Setup.exe)
- [![Linux](https://img.shields.io/badge/latest_version-%23E95420?style=plastic&logo=ubuntu&logoColor=%23E95420&label=Linux)](https://gitlab.com/Nootaku/picpicker/-/jobs/3408074571/artifacts/raw/out/v1.2.0/make/deb/x64/picpicker_1.2.0_amd64.deb)

> Please note that the installation on **_Windows_** will result in a scary-looking warning asking you to confirm that you trust me as a developper before installing.<br/>
> Since this is a personal project that I am willing to offer for free, I am not ready to purchase a [Code Signing Certificate](https://sectigo.com/ssl-certificates-tls/code-signing).
>
> If you want to double-check the contents of the application feel free to have a look at the _PicPicker_ repository.

<br/>![](./documentation/images/pp_readme.png)

### Why should I use it ?

I created this application when I came back from holidays and had to go through 1000+ images and sort them for an album. Quickly, I noticed that I was manually copy-pasting individual images from my SD card to a specific folder on my computer. Moreover the names my camera gave to those images were similar to `DCIM127_20200424553.jpg`. Which made it very inconvenient to distinguish two similar pictures, one blurry, one focused without actually paying attention to the task.

Enters _PicPicker_ !<br/>Since you can see the pictures in the viewer, you can remove  the hassle of copy-pasting.

### With useful keyboard shortcuts

Once your folders are selected, you don't need your mouse. `Left` and `Right` arrow keys for parsing pictures, `Up` and `Down` arrow keys for rotation and `Space` to copy.

## Developer guide

### Installation

This installation guide assumes that you have [`Node.js`](https://nodejs.org/en/download/) and [`npm`](https://docs.npmjs.com/) installed. Visit their respective sites to find the documentation on how to install them if it is not the case.

```bash
# Clone the project
git clone https://gitlab.com/Nootaku/picpicker.git
cd picpicker

# Install the dependencies
npm install
```

### Run the app

```bash
npm run start
```

### Build the app

[See Wiki](https://gitlab.com/Nootaku/picpicker/-/wikis/Package-Application)

## Limitations

This program is still a work in progress. Even thought this first version is perfectly functional, some visual bugs are possible, including:

- Once rotated pictures may lose their ratio

